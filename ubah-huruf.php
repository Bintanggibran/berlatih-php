<?php
function ubah_huruf($string){
    $string1 = "";
    $alphabet = array('a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z');
    for($i = 0; $i < strlen($string); $i++){
        for ($j=0; $j < count($alphabet); $j++) { 
            if($string[$i] == 'z'){
                $string1.=$alphabet[0];
            }else if($string[$i] == $alphabet[$j]){
                $string1.=$alphabet[$j+1];
            }
        }
    }
    echo "<br>";
    return $string1;
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu

?>