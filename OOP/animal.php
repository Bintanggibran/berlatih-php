<?php
    class Animal{
        public $name;
        public $legs = 2;
        public $cold_blooded = "no";

        public function __construct($name) {
            $this->name = $name;
        }
        public function set_legs($legs){
            $this -> legs = $legs;
        }
        public function set_cold_blooded($cold_blooded){
            $this -> cold_blooded = $cold_blooded;
        }
        public function get_name(){
            echo "<br>";
            return $this->name;
        }
        public function get_legs(){
            echo "<br>";
            return $this -> legs;
        }
        public function get_cold_blooded(){
            echo "<br>";
            return $this -> cold_blooded;
        }
    }

    class Ape extends Animal{
        public function yell(){
            echo "<br>Yell: Auooo<br>";
        }
    }

    class Frog extends Animal{
        public function jump(){
            echo "<br>Jump: hop hop<br>";
        }
    }
?>