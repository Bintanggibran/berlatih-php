<?php
include "animal.php";

$sheep = new Animal("shaun");
$sheep ->set_legs(4);
$sheep ->set_cold_blooded("no");

echo "Name: ".$sheep->get_name(); // "shaun"
echo "legs: ".$sheep->get_legs(); // 4
echo "cold blooded: ".$sheep->get_cold_blooded(); echo "<br>"; // "no"

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())
$sungokong = new Ape("kera sakti");
echo "Name: ".$sungokong -> get_name();
echo "legs: ".$sungokong -> get_legs();
echo "cold blooded: ".$sungokong -> get_cold_blooded();
$sungokong -> yell(); // "Auooo"


$kodok = new Frog("buduk");
$kodok -> set_legs(4);
echo "Name: ".$kodok -> get_name();
echo "legs: ".$kodok -> get_legs();
echo "cold blooded: ".$kodok -> get_cold_blooded();
$kodok->jump() ; // "hop hop"
?>